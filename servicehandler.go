// servicehandler.go
//
// license--start
//
// Copyright 2018 John Marshall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// license--end

package goruss

import ()

/*
#include <stdlib.h>
#include <russ/priv.h>

extern void goruss_common_service_handler(struct russ_sess *);
*/
import "C"

type ServiceHandler func(*Session)

var serviceHandlers = map[*C.struct_russ_svcnode]ServiceHandler{}

//export commonGoServiceHandler
//
// Helper to call Go-side handler based on russ_sess pointer
// registered with storeServiceHandler.
func commonGoServiceHandler(cSess *C.struct_russ_sess) {
	root := NewServiceNode(cSess.svr.root)
	spath := C.GoString(cSess.req.spath)
	node, _ := root.Find(spath)
	handler := getServiceHandler(node.p)
	if handler != nil {
		sess := NewSession(cSess)
		handler(sess)
	}
}

// Return go-side service handler associated with rss_svcnode
// pointer.
func getServiceHandler(cNode *C.struct_russ_svcnode) ServiceHandler {
	if handler, ok := serviceHandlers[cNode]; ok {
		return handler
	}
	return nil
}

// Storage reference to go-side service handler keyed by
// russ_svcnode pointer.
func storeServiceHandler(cNode *C.struct_russ_svcnode, handler ServiceHandler) {
	serviceHandlers[cNode] = handler
}
