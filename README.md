# goruss

goruss provides a go interface to the C RUSS library.

To use goruss, it is necessary to have the C RUSS library available.
see Web link below for details.

# Contact and Other Information

* Author: John Marshall
* Groups: http://groups.google.com/group/russ-users
* Repository: https://bitbucket.org/russng/goruss
* Web: https://expl.info/display/RUSS
