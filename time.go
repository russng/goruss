// time.go
//
// license--start
//
// Copyright 2018 John Marshall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// license--end

package goruss

/*
#include <russ/russ.h>
*/
import "C"

// Time value represented as a succiently-sized integer.
type Deadline int64

// Get current time as a Deadline.
func GetTime() Deadline {
	return Deadline(C.russ_gettime())
}

// Convert timeout (from now) to Deadline.
func ToDeadline(timeout int) Deadline {
	return Deadline(C.russ_to_deadline(C.int(timeout)))
}

// Get difference between a Deadline value and now (as a Deadline).
func ToDeadlineDiff(deadline Deadline) Deadline {
	return Deadline(C.russ_to_deadlinediff(C.russ_deadline(deadline)))
}

// Convert a Deadline value to timeout.
func ToTimeout(deadline Deadline) int {
	return int(C.russ_to_timeout(C.russ_deadline(deadline)))
}
