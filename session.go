// session.go
//
// license--start
//
// Copyright 2018 John Marshall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// license--end

package goruss

import ()

/*
#include <stdlib.h>
#include <russ/priv.h>
*/
import "C"

type Session struct {
	p *C.struct_russ_sess
}

func NewSession(p *C.struct_russ_sess) *Session {
	s := &Session{}
	s.p = p
	return s
}

func (s *Session) FreeSession() {
	s.p = nil
}

func (s *Session) GetRequest() *Request {
	if s.p.req == nil {
		return nil
	}
	return NewRequest(s.p.req)
}

func (s *Session) GetServer() *Server {
	if s.p.svr == nil {
		return nil
	}
	svr := &Server{}
	svr.p = s.p.svr
	return svr
}

func (s *Session) GetServerConn() *ServerConn {
	if s.p.sconn == nil {
		return nil
	}
	return NewServerConn(s.p.sconn)
}

func (s *Session) GetSpath() string {
	if s.p.req == nil {
		return ""
	}
	return C.GoString(s.p.req.spath)
}
