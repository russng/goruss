// chelpers.go
//
// license--start
//
// Copyright 2018 John Marshall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// license--end

package goruss

/*
#cgo LDFLAGS: -lruss
//#cgo LDFLAGS: /usr/lib/libruss.a

#include <stdlib.h>
#include <russ/russ.h>

// pack/unpack helpers
char
**goruss_makeCharArray(int size) {
	return calloc(sizeof(char *), size);
}

void
goruss_set_array_string(char **a, char *s, int n) {
	//fprintf(stderr, "n (%d) s (%p)\n", n, s);
	a[n] = s;
}

char *
goruss_get_ref_array_string(char **a, int pos) {
	return a[pos];
}

char **
goruss_free_array_elements(char **a) {
	char	**p;

	for (p = a; *p != NULL; p++) {
		free(*p);
		*p = NULL;
	}
	free(a);
	return NULL;
}

char **
goruss_free_narray_elements(int n, char **a) {
	int i;

	for (i = 0; i < n; i++) {
		free(a[i]);
		a[i] = NULL;
	}
	free(a);
	return NULL;
}

extern void goruss_common_service_handler(struct russ_sess *);
extern void commonGoServiceHandler(struct russ_sess *);

void
goruss_common_service_handler(struct russ_sess *sess) {
	commonGoServiceHandler(sess);
}
*/
import "C"

/* go-based pack/unpack helpers */
func mapToCStringArray(m map[string]string) **C.char {
	p := C.goruss_makeCharArray(C.int(len(m) + 1))
	i := 0
	for k, v := range m {
		s := k + "=" + v
		C.goruss_set_array_string(p, C.CString(s), C.int(i))
		i += 1
	}
	C.goruss_set_array_string(p, nil, C.int(i))
	return p
}

func stringArrayToCStringArray(a []string) **C.char {
	p := C.goruss_makeCharArray(C.int(len(a) + 1))
	i := 0
	for _, v := range a {
		C.goruss_set_array_string(p, C.CString(v), C.int(i))
		i += 1
	}
	C.goruss_set_array_string(p, nil, C.int(i))
	return p
}

func getRefCStringArrayValue(a **C.char, pos int) *C.char {
	return C.goruss_get_ref_array_string(a, C.int(pos))
}

func freeCStringArray(a **C.char) **C.char {
	return C.goruss_free_array_elements(a)
}

func freeCStringNArray(n int, a **C.char) **C.char {
	return C.goruss_free_narray_elements(C.int(n), a)
}
