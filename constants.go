// constants.go
//
// license--start
//
// Copyright 2018 John Marshall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// license--end

package goruss

/*
#include <russ/russ.h>
*/
import "C"

// For connection fds.
const (
	RUSS_CONN_NFDS      int = C.RUSS_CONN_NFDS
	RUSS_CONN_STD_NFDS  int = C.RUSS_CONN_STD_NFDS
	RUSS_CONN_FD_STDIN  int = C.RUSS_CONN_FD_STDIN
	RUSS_CONN_FD_STDOUT int = C.RUSS_CONN_FD_STDOUT
	RUSS_CONN_FD_STDERR int = C.RUSS_CONN_FD_STDERR

	RUSS_CONN_NSYSFDS    int = C.RUSS_CONN_NSYSFDS
	RUSS_CONN_SYSFD_EXIT int = C.RUSS_CONN_SYSFD_EXIT

	RUSS_CONN_MAX_NFDS int = C.RUSS_CONN_MAX_NFDS
)

// For deadline.
const (
	RUSS_DEADLINE_NEVER Deadline = C.RUSS_DEADLINE_NEVER
)

// Standardized exit statuses.
const (
	RUSS_EXIT_SUCCESS     int = C.RUSS_EXIT_SUCCESS
	RUSS_EXIT_FAILURE     int = C.RUSS_EXIT_FAILURE
	RUSS_EXIT_CALLFAILURE int = C.RUSS_EXIT_CALLFAILURE
	RUSS_EXIT_SYSFAILURE  int = C.RUSS_EXIT_SYSFAILURE
)

// Standardized messages.
const (
	RUSS_MSG_BADARGS       = C.RUSS_MSG_BADARGS
	RUSS_MSG_BADCONNEVENT  = C.RUSS_MSG_BADCONNEVENT
	RUSS_MSG_BADOP         = C.RUSS_MSG_BADOP
	RUSS_MSG_NODIAL        = C.RUSS_MSG_NODIAL
	RUSS_MSG_NOEXIT        = C.RUSS_MSG_NOEXIT
	RUSS_MSG_NOLIST        = C.RUSS_MSG_NOLIST
	RUSS_MSG_NOSERVICE     = C.RUSS_MSG_NOSERVICE
	RUSS_MSG_NOSWITCH_USER = C.RUSS_MSG_NOSWITCHUSER
	RUSS_MSG_UNDEFSERVICE  = C.RUSS_MSG_UNDEFSERVICE
)

// Request operation numbers.
const (
	RUSS_OPNUM_NOTSET    int = C.RUSS_OPNUM_NOTSET
	RUSS_OPNUM_EXTENSION int = C.RUSS_OPNUM_EXTENSION
	RUSS_OPNUM_EXECUTE   int = C.RUSS_OPNUM_EXECUTE
	RUSS_OPNUM_HELP      int = C.RUSS_OPNUM_HELP
	RUSS_OPNUM_ID        int = C.RUSS_OPNUM_ID
	RUSS_OPNUM_INFO      int = C.RUSS_OPNUM_INFO
	RUSS_OPNUM_LIST      int = C.RUSS_OPNUM_LIST
)

// For Request.
const (
	RUSS_REQ_ARGS_MAX       int = C.RUSS_REQ_ARGS_MAX
	RUSS_REQ_ATTRS_MAX      int = C.RUSS_REQ_ATTRS_MAX
	RUSS_REQ_SPATH_MAX      int = C.RUSS_REQ_SPATH_MAX
	RUSS_REQ_PROTOCOLSTRING     = C.RUSS_REQ_PROTOCOLSTRING
)

// For Server.
const (
	RUSS_SVR_LIS_SD_DEFAULT int = C.RUSS_SVR_LIS_SD_DEFAULT
	RUSS_SVR_TIMEOUT_ACCEPT int = C.RUSS_SVR_TIMEOUT_ACCEPT
	RUSS_SVR_TIMEOUT_AWAIT  int = C.RUSS_SVR_TIMEOUT_AWAIT
	RUSS_SVR_TYPE_FORK      int = C.RUSS_SVR_TYPE_FORK
	RUSS_SVR_TYPE_THREAD    int = C.RUSS_SVR_TYPE_THREAD

	RUSS_SERVICES_DIR = C.RUSS_SERVICES_DIR
)

// Return values for ClientConn.Wait.
const (
	RUSS_WAIT_UNSET   int = C.RUSS_WAIT_UNSET
	RUSS_WAIT_OK      int = C.RUSS_WAIT_OK
	RUSS_WAIT_FAILURE int = C.RUSS_WAIT_FAILURE
	RUSS_WAIT_BADFD   int = C.RUSS_WAIT_BADFD
	RUSS_WAIT_TIMEOUT int = C.RUSS_WAIT_TIMEOUT
	RUSS_WAIT_HUP     int = C.RUSS_WAIT_HUP
)
