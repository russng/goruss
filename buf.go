// buf.go
//
// license--start
//
// Copyright 2018 John Marshall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// license--end

package goruss

import "unsafe"

/*
#include <russ/russ.h>
*/
import "C"

type Buf struct {
	p *C.struct_russ_buf
}

func NewBuf(size int) *Buf {
	b := &Buf{}
	if b.p = C.russ_buf_new(C.int(size)); b.p == nil {
		return nil
	}
	return b
}

func (b *Buf) FreeBuf() {
	if b.p != nil {
		C.russ_buf_free(b.p)
		b.p = nil
	}
}

func (b *Buf) GetData() []byte {
	data := make([]byte, int(b.p.len))
	copy(data, C.GoBytes(unsafe.Pointer(b.p.data), b.p.len))
	return data
}

func (b *Buf) PutData(data []byte) {
	bdata := C.GoBytes(unsafe.Pointer(b.p.data), b.p.len)
	copy(bdata, data)
}
