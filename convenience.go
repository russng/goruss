// convenience.go
//
// license--start
//
// Copyright 2018 John Marshall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// license--end

package goruss

/*
#cgo LDFLAGS: -lruss

#include <stdlib.h>
#include <russ/russ.h>
*/
import "C"

import "errors"

type DialWaitInOutErrResult struct {
	Rv   int
	Ev   int
	Sout string
	Serr string
}

// Convenience function for Dialv.
func DialvWait(deadline Deadline, op string, spath string, attrs map[string]string, args []string) (DialWaitInOutErrResult, error) {
	res := DialWaitInOutErrResult{}

	cOp, cSPath, cAttrs, cArgs := newDialParamsToC(op, spath, attrs, args)
	defer freeDialParamsToC(cOp, cSPath, cAttrs, cArgs)
	exit_status := C.int(0)

	res.Rv = int(C.russ_dialv_wait(C.russ_deadline(deadline), cOp, cSPath, cAttrs, cArgs, &exit_status))
	res.Ev = int(exit_status)
	res.Sout = ""
	res.Serr = ""

	if res.Rv == 0 {
		return res, nil
	} else {
		return res, errors.New("dial failure")
	}
}

// Convenience function for Dialv.
func DialvWaitTimeout(timeout int, op string, spath string, attrs map[string]string, args []string) (DialWaitInOutErrResult, error) {
	return DialvWait(ToDeadline(timeout), op, spath, attrs, args)
}

// Convenience function for Dialv.
func DialvWaitInOutErr(deadline Deadline, op string, spath string, attrs map[string]string, args []string, stdin string, stdout_size, stderr_size int) (DialWaitInOutErrResult, error) {
	res := DialWaitInOutErrResult{}

	cOp, cSPath, cAttrs, cArgs := newDialParamsToC(op, spath, attrs, args)
	defer freeDialParamsToC(cOp, cSPath, cAttrs, cArgs)
	exit_status := C.int(0)

	// setup buffers
	rbufs := make([]*C.struct_russ_buf, 3)
	rbufs[0] = C.russ_buf_new(C.int(len(stdin)))
	C.russ_buf_set(rbufs[0], C.CString(stdin), C.int(len(stdin)))
	rbufs[1] = C.russ_buf_new(C.int(stdout_size))
	rbufs[2] = C.russ_buf_new(C.int(stderr_size))
	defer C.russ_buf_free(rbufs[0])
	defer C.russ_buf_free(rbufs[1])
	defer C.russ_buf_free(rbufs[2])

	res.Rv = int(C.russ_dialv_wait_inouterr3(C.russ_deadline(deadline), cOp, cSPath,
		cAttrs, cArgs, &exit_status, rbufs[0], rbufs[1], rbufs[2]))
	res.Ev = int(exit_status)
	res.Sout = C.GoStringN(rbufs[1].data, rbufs[1].len)
	res.Serr = C.GoStringN(rbufs[2].data, rbufs[2].len)

	if res.Rv == 0 {
		return res, nil
	} else {
		return res, errors.New("dial failure")
	}
}

// Convenience function for Dialv.
func DialvWaitInOutErrTimeout(timeout int, op string, spath string, attrs map[string]string, args []string, stdin string, stdout_size, stderr_size int) (DialWaitInOutErrResult, error) {
	return DialvWaitInOutErr(ToDeadline(timeout), op, spath, attrs, args, stdin, stdout_size, stderr_size)
}

// Convenience function for Dialv.
func Execv(deadline Deadline, spath string, attrs map[string]string, args []string) *ClientConn {
	return Dialv(deadline, "execute", spath, attrs, args)
}

// Convenience function for Execv.
func ExecvWait(deadline Deadline, spath string, attrs map[string]string, args []string) (DialWaitInOutErrResult, error) {
	return DialvWait(deadline, "execute", spath, attrs, args)
}

// Convenience function for Execv.
func ExecvWaitInOutErr(deadline Deadline, spath string, attrs map[string]string, args []string, stdin string, stdout_size, stderr_size int) (DialWaitInOutErrResult, error) {
	return DialvWaitInOutErr(deadline, "execute", spath, attrs, args, stdin, stdout_size, stderr_size)
}

// Convenience function for Execv.
func ExecvWaitInOutErrTimeout(timeout int, spath string, attrs map[string]string, args []string, stdin string, stdout_size, stderr_size int) (DialWaitInOutErrResult, error) {
	return DialvWaitInOutErrTimeout(timeout, "execute", spath, attrs, args, stdin, stdout_size, stderr_size)
}
