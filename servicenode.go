// servicenode.go
//
// license--start
//
// Copyright 2018 John Marshall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// license--end

package goruss

import (
	"errors"
	"unsafe"
)

/*
#include <stdlib.h>
#include <russ/priv.h>

extern void goruss_common_service_handler(struct russ_sess *);
*/
import "C"

type ServiceNode struct {
	p *C.struct_russ_svcnode
}

func NewServiceNode(p *C.struct_russ_svcnode) *ServiceNode {
	s := &ServiceNode{}
	s.p = p
	return s
}

func NewRootServiceNode(name string, handler ServiceHandler) *ServiceNode {
	cName := C.CString(name)
	defer C.free(unsafe.Pointer(cName))
	p := C.russ_svcnode_new(cName, C.russ_svchandler(C.goruss_common_service_handler))
	if p == nil {
		return nil
	}
	s := NewServiceNode(p)
	storeServiceHandler(s.p, handler)
	return s
}

func (s *ServiceNode) FreeServiceNode() {
	C.russ_svcnode_free(s.p)
	s.p = nil
}

func (s *ServiceNode) Add(name string, handler ServiceHandler) *ServiceNode {
	cName := C.CString(name)
	defer C.free(unsafe.Pointer(cName))
	childp := C.russ_svcnode_add(s.p, cName, C.russ_svchandler(C.goruss_common_service_handler))
	if childp == nil {
		return nil
	}
	child := NewServiceNode(childp)
	storeServiceHandler(childp, handler)
	return child
}

func (s *ServiceNode) Find(path string) (*ServiceNode, string) {
	cPath := C.CString(path)
	defer C.free(unsafe.Pointer(cPath))
	// TODO: allocate char * memory of 1024 bytes
	mpath_cap := 1024
	cMpath := (*C.char)(C.malloc(C.size_t(mpath_cap)))
	defer C.free(unsafe.Pointer(cMpath))

	if p := C.russ_svcnode_find(s.p, cPath, cMpath, C.int(mpath_cap)); p == nil {
		return nil, ""
	} else {
		s := &ServiceNode{}
		s.p = p
		mpath := C.GoString(cMpath)
		return s, mpath
	}
}

func (s *ServiceNode) SetAutoanswer(value int) error {
	if int(C.russ_svcnode_set_autoanswer(s.p, C.int(value))) < 0 {
		return errors.New("failed to set autoanswer")
	}
	return nil
}

func (s *ServiceNode) SetHandler(handler ServiceHandler) error {
	if C.russ_svcnode_set_handler(s.p, C.russ_svchandler(C.goruss_common_service_handler)) < 0 {
		return errors.New("failed to set service node handler")
	}
	// TODO: drop old handler in lookup?
	storeServiceHandler(s.p, handler)
	return nil
}

func (s *ServiceNode) SetVirtual(value int) error {
	if int(C.russ_svcnode_set_virtual(s.p, C.int(value))) < 0 {
		return errors.New("failed to set virtual")
	}
	return nil
}

func (s *ServiceNode) SetWildcard(value int) error {
	if int(C.russ_svcnode_set_wildcard(s.p, C.int(value))) < 0 {
		return errors.New("failed to set wildcard")
	}
	return nil
}
