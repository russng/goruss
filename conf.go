// conf.go
//
// license--start
//
// Copyright 2018 John Marshall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// license--end

package goruss

import (
	"errors"
	"unsafe"
)

/*
#include <stdlib.h>
#include <russ/russ.h>
*/
import "C"

type Conf struct {
	p *C.struct_russ_conf
}

func NewConf(p *C.struct_russ_conf) *Conf {
	c := &Conf{}
	c.p = p
	return c
}

func (c *Conf) FreeConf() {
	C.russ_conf_free(c.p)
	c.p = nil
}

func InitConf(argv []string) (*Conf, []string) {
	return LoadConf(argv)
}

func LoadConf(argv []string) (*Conf, []string) {
	cArgc := C.int(len(argv))
	cArgv := stringArrayToCStringArray(argv)
	defer freeCStringArray(cArgv)

	p := C.russ_conf_load(&cArgc, cArgv)
	if p == nil {
		return nil, argv

	}
	c := NewConf(p)

	// update copy of argv
	for i := 0; ; i++ {
		if cValue := getRefCStringArrayValue(cArgv, i); cValue == nil {
			argv = argv[:i]
			break
		} else {
			argv[i] = C.GoString(cValue)
		}
	}

	return c, argv
}

func (c *Conf) AddSection(sectionName string) error {
	cSectionName := C.CString(sectionName)
	defer C.free(unsafe.Pointer(cSectionName))
	if C.russ_conf_add_section(c.p, cSectionName) < 0 {
		return errors.New("failed to add section")
	}
	return nil
}

func (c *Conf) DupSection(srcSectionName, dstSectionName string) error {
	cSrcSectionName := C.CString(srcSectionName)
	cDstSectionName := C.CString(dstSectionName)
	defer C.free(unsafe.Pointer(cSrcSectionName))
	defer C.free(unsafe.Pointer(cDstSectionName))
	if C.russ_conf_dup_section(c.p, cSrcSectionName, cDstSectionName) < 0 {
		return errors.New("failed to duplication section")
	}
	return nil
}

func (c *Conf) Get(sectionName, option, dvalue string) string {
	cSectionName := C.CString(sectionName)
	cOption := C.CString(option)
	cDValue := C.CString(dvalue)
	defer C.free(unsafe.Pointer(cSectionName))
	defer C.free(unsafe.Pointer(cOption))
	defer C.free(unsafe.Pointer(cDValue))
	cRv := C.russ_conf_get(c.p, cSectionName, cOption, cDValue)
	defer C.free(unsafe.Pointer(cRv))
	return C.GoString(cRv)
}

func (c *Conf) GetInt(sectionName, option string, dvalue int64) int64 {
	cSectionName := C.CString(sectionName)
	cOption := C.CString(option)
	cDValue := C.long(dvalue)
	defer C.free(unsafe.Pointer(cSectionName))
	defer C.free(unsafe.Pointer(cOption))
	return int64(C.russ_conf_getint(c.p, cSectionName, cOption, cDValue))
}

func (c *Conf) GetFloat(sectionName, option string, dvalue float64) float64 {
	cSectionName := C.CString(sectionName)
	cOption := C.CString(option)
	cDValue := C.double(dvalue)
	defer C.free(unsafe.Pointer(cSectionName))
	defer C.free(unsafe.Pointer(cOption))
	return float64(C.russ_conf_getfloat(c.p, cSectionName, cOption, cDValue))
}

func (c *Conf) GetSInt(sectionName, option string, dvalue int64) int64 {
	cSectionName := C.CString(sectionName)
	cOption := C.CString(option)
	cDValue := C.long(dvalue)
	defer C.free(unsafe.Pointer(cSectionName))
	defer C.free(unsafe.Pointer(cOption))
	return int64(C.russ_conf_getint(c.p, cSectionName, cOption, cDValue))
}

func (c *Conf) HasOption(sectionName, option string) bool {
	cSectionName := C.CString(sectionName)
	cOption := C.CString(option)
	defer C.free(unsafe.Pointer(cSectionName))
	defer C.free(unsafe.Pointer(cOption))
	if C.russ_conf_has_option(c.p, cSectionName, cOption) == 1 {
		return true
	}
	return false
}

func (c *Conf) HasSection(sectionName string) bool {
	cSectionName := C.CString(sectionName)
	defer C.free(unsafe.Pointer(cSectionName))
	if C.russ_conf_has_section(c.p, cSectionName) == 1 {
		return true
	}
	return false
}

func (c *Conf) Options(sectionName string) []string {
	options := []string{}

	cSectionName := C.CString(sectionName)
	cOptions := C.russ_conf_options(c.p, cSectionName)
	defer freeCStringArray(cOptions)
	for i := 0; ; i++ {
		if cValue := getRefCStringArrayValue(cOptions, i); cValue == nil {
			break
		} else {
			options = append(options, C.GoString(cValue))
		}
	}

	return options
}

func (c *Conf) Read(filename string) error {
	cFilename := C.CString(filename)

	if C.russ_conf_read(c.p, cFilename) < 0 {
		return errors.New("failedname to read file")
	}
	return nil
}

func (c *Conf) RemoveOption(sectionName, option string) error {
	cSectionName := C.CString(sectionName)
	cOption := C.CString(option)
	defer C.free(unsafe.Pointer(cSectionName))
	defer C.free(unsafe.Pointer(cOption))
	if C.russ_conf_remove_option(c.p, cSectionName, cOption) < 0 {
		return errors.New("failed to remove option")
	}
	return nil
}

func (c *Conf) RemoveSection(sectionName string) error {
	cSectionName := C.CString(sectionName)
	defer C.free(unsafe.Pointer(cSectionName))
	if C.russ_conf_remove_section(c.p, cSectionName) < 0 {
		return errors.New("failed to remove section")
	}
	return nil
}

func (c *Conf) Sections() []string {
	sections := []string{}

	cSections := C.russ_conf_sections(c.p)
	defer freeCStringArray(cSections)
	for i := 0; ; i++ {
		if cValue := getRefCStringArrayValue(cSections, i); cValue == nil {
			break
		} else {
			sections = append(sections, C.GoString(cValue))
		}
	}

	return sections
}

func (c *Conf) Set(sectionName, option, value string) error {
	cSectionName := C.CString(sectionName)
	cOption := C.CString(option)
	cValue := C.CString(value)

	if C.russ_conf_set(c.p, cSectionName, cOption, cValue) < 0 {
		return errors.New("failed to set option")
	}
	return nil
}

func (c *Conf) Set2(sectionName, option, value string) error {
	cSectionName := C.CString(sectionName)
	cOption := C.CString(option)
	cValue := C.CString(value)

	if C.russ_conf_set2(c.p, cSectionName, cOption, cValue) < 0 {
		return errors.New("failed to set option")
	}
	return nil
}

func (c *Conf) Write(filename string) error {
	return errors.New("failed to write to file")
}
