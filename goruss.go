// goruss.go
//
// license--start
//
// Copyright 2018 John Marshall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// license--end

/*
Package goruss provides an interface to the C russ package.

The current implementation only supports client connections.
Server support will come later.

Client Connections

All clients depend on one or the other versions of the Dialv
call:
	conn := russ.Dialv(russ.ToDeadline(10000),
		"execute",
		"+/debug/daytime",
		nil,
		nil)

Variations of the Dialv call are available as helpers. E.g.,
	rv, ev, sout, serr := russ.DialvWaitInOutErrTimeout(
		10000,
		"execute",
		nil,
		nil,
		"", 1<<20, 1<<20)
*/

package goruss
