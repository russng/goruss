// clientconn.go
//
// license--start
//
// Copyright 2018 John Marshall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// license--end

package goruss

import (
	"unsafe"
)

/*
#include <stdlib.h>
#include <russ/russ.h>
*/
import "C"

// Base client connection object
type ClientConn struct {
	p *C.struct_russ_cconn
}

func newDialParamsToC(op string, spath string, attrs map[string]string, args []string) (*C.char, *C.char, **C.char, **C.char) {
	cOp := C.CString(op)
	cSPath := C.CString(spath)
	cAttrs := mapToCStringArray(attrs)
	cArgs := stringArrayToCStringArray(args)
	return cOp, cSPath, cAttrs, cArgs
}

func freeDialParamsToC(cOp *C.char, cSPath *C.char, cAttrs **C.char, cArgs **C.char) {
	C.free(unsafe.Pointer(cOp))
	C.free(unsafe.Pointer(cSPath))
	freeCStringArray(cAttrs)
	freeCStringArray(cArgs)
}

/*
Dialv is the fundamental function for setting up a connection to a
RUSS server.

See convenience functions, elsewhere, such as DialvWaitInOutErr and
the like.
*/
func Dialv(deadline Deadline, op string, spath string, attrs map[string]string, args []string) *ClientConn {
	cOp, cSPath, cAttrs, cArgs := newDialParamsToC(op, spath, attrs, args)
	defer freeDialParamsToC(cOp, cSPath, cAttrs, cArgs)

	p := C.russ_dialv(C.russ_deadline(deadline), cOp, cSPath, cAttrs, cArgs)
	if p == nil {
		return nil
	}
	return NewClientConn(p)
}

// See Dialv.
func Diall(deadline Deadline, op string, spath string, attrs map[string]string, args ...string) *ClientConn {
	return Dialv(deadline, op, spath, attrs, args)
}

func NewClientConn(p *C.struct_russ_cconn) *ClientConn {
	// dialer is owner
	c := &ClientConn{}
	c.p = p
	return c
}

func (c *ClientConn) FreeClientConn() {
	// dialer is owner
	C.russ_cconn_free(c.p)
	c.p = nil
}

// Close connection.
func (c *ClientConn) Close() {
	C.russ_cconn_close(c.p)
}

// Close fd (by index) associated with connection.
func (c *ClientConn) CloseFd(i int) {
	C.russ_cconn_close_fd(c.p, C.int(i))
}

// Get fd (by index) associated with connection.
func (c *ClientConn) GetFd(i int) int {
	return int(c.p.fds[i])
}

// Get array of fds associated with connection.
func (c *ClientConn) GetFds() []int {
	var fds []int

	for i := 0; i < C.RUSS_CONN_NFDS; i++ {
		fds = append(fds, int(c.p.fds[i]))
	}
	return fds
}

// Get socket descriptor associated with connection.
func (c *ClientConn) GetSd() int {
	return int(c.p.sd)
}

// Get sysfd (by index) associated with connection
func (c *ClientConn) GetSysFd(index int) int {
	if index < 0 || index > RUSS_CONN_NSYSFDS {
		return -1
	}
	return int(c.p.sysfds[C.int(index)])
}

// Get array of sysfds associated with connection.
func (c *ClientConn) GetSysFds() []int {
	var fds []int

	for i := 0; i < C.RUSS_CONN_NSYSFDS; i++ {
		fds = append(fds, int(c.p.sysfds[i]))
	}
	return fds
}

// Set fd (by index) associated with connection.
func (c *ClientConn) SetFd(i, fd int) {
	c.p.fds[i] = C.int(fd)
}

// Wait for exit value to be returned. The function returns a value
// indicating success/failure (see RUSS_WAIT_* constants) of the
// function call and the exit value.
func (c *ClientConn) Wait(deadline Deadline) (int, int) {
	exitStatus := C.int(0)
	return int(C.russ_cconn_wait(c.p, C.russ_deadline(deadline), &exitStatus)), int(exitStatus)
}
