// serverconn.go
//
// license--start
//
// Copyright 2018 John Marshall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// license--end

package goruss

import (
	"errors"
	"unsafe"
)

/*
#include <stdlib.h>
#include <russ/priv.h>

// remove for russng 6.4
void russ_sconn_close_fd(struct russ_sconn *, int);
*/
import "C"

type ServerConn struct {
	p *C.struct_russ_sconn
}

func NewServerConn(p *C.struct_russ_sconn) *ServerConn {
	s := &ServerConn{}
	s.p = p
	return s
}

// release underlying object; rarely useful from go
func (s *ServerConn) FreeServerConn() {
	C.russ_sconn_free(s.p)
	s.p = nil
}

// Accept not available
// AcceptHandler available

func (s *ServerConn) Answer(nfds int, cfds []int) error {
	return errors.New("not available")
	return nil
}

// AnswerHandler not available

func (s *ServerConn) AwaitRequest(deadline Deadline) *Request {
	p := C.russ_sconn_await_req(s.p, C.russ_deadline(deadline))
	return NewRequest(p)
}

func (s *ServerConn) Close() {
	C.russ_sconn_close(s.p)
}

func (s *ServerConn) CloseFd(index int) {
	C.russ_sconn_close_fd(s.p, C.int(index))
}

func (s *ServerConn) GetCreds() *Credentials {
	return NewCredentials(int(s.p.creds.uid), int(s.p.creds.gid), int(s.p.creds.pid))
}

func (s *ServerConn) GetFd(index int) int {
	if index < 0 || index > RUSS_CONN_NFDS {
		return -1
	}
	return int(s.p.fds[C.int(index)])
}

// Get array of fds associated with connection.
func (s *ServerConn) GetFds() []int {
	var fds []int

	for i := 0; i < C.RUSS_CONN_NFDS; i++ {
		fds = append(fds, int(s.p.fds[i]))
	}
	return fds
}

// Get sysfd (by index) associated with connection.
func (s *ServerConn) GetSysFd(index int) int {
	if index < 0 || index > RUSS_CONN_NSYSFDS {
		return -1
	}
	return int(s.p.sysfds[C.int(index)])
}

// Get array of sysfds associated with connection.
func (s *ServerConn) GetSysFds() []int {
	var fds []int

	for i := 0; i < C.RUSS_CONN_NSYSFDS; i++ {
		fds = append(fds, int(s.p.sysfds[i]))
	}
	return fds
}

func (s *ServerConn) Exit(value int) error {
	ev := int(C.russ_sconn_exit(s.p, C.int(value)))
	if ev < 0 {
		return errors.New("failed to send exit value")
	}
	return nil
}

func (s *ServerConn) Fatal(msg string, value int) error {
	cMsg := C.CString(msg)
	defer C.free(unsafe.Pointer(cMsg))

	ev := int(C.russ_sconn_fatal(s.p, cMsg, C.int(value)))
	if ev < 0 {
		return errors.New("failed to send message/exit value")
	}
	return nil
}

func (s *ServerConn) RedialAndSplice(deadline Deadline, req *Request) error {
	ev := C.russ_sconn_redialandsplice(s.p, C.russ_deadline(deadline), req.p)
	if ev < 0 {
		return errors.New("failed to redial and splice")
	}
	return nil
}

// SendFds not available

func (s *ServerConn) Splice(dconn *ClientConn) error {
	ev := C.russ_sconn_splice(s.p, dconn.p)
	if ev < 0 {
		return errors.New("failed to splice")
	}
	return nil
}
