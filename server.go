// server.go
//
// license--start
//
// Copyright 2018 John Marshall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// license--end

package goruss

import (
	"errors"
	"syscall"
	"unsafe"
)

/*
#include <stdlib.h>
#include <russ/priv.h>

// remove for russng 6.4
int russ_svr_set_lisd(struct russ_svr *, int);
*/
import "C"

type Server struct {
	p *C.struct_russ_svr
}

func InitServer(conf *Conf) *Server {
	if conf == nil {
		return nil
	}

	sd := int(conf.GetInt("main", "sd", int64(RUSS_SVR_LIS_SD_DEFAULT)))
	accepttimeout := int(conf.GetInt("main", "accepttimeout", int64(RUSS_SVR_TIMEOUT_ACCEPT)))
	closeonaccept := int(conf.GetInt("main", "closeonaccept", int64(0)))
	root := NewRootServiceNode("", nil)
	if root == nil {
		return nil
	}
	svr := NewServer(root, 0, sd)
	if svr == nil ||
		svr.SetAccepttimeout(accepttimeout) != nil ||
		svr.SetCloseonaccept(closeonaccept) != nil {
		svr.FreeServer()
		return nil
	}
	return svr
}

func NewServer(root *ServiceNode, typ int, lisd int) *Server {
	p := C.russ_svr_new(root.p, C.int(typ), C.int(lisd))
	if p == nil {
		return nil
	}
	s := &Server{}
	s.p = p
	return s
}

// release underlying object; rarely used from go
func (s *Server) FreeServer() {
	C.russ_svr_free(s.p)
	s.p = nil
}

func (s *Server) Accept(deadline Deadline) *ServerConn {
	sconnp := C.russ_svr_accept(s.p, C.russ_deadline(deadline))
	if sconnp == nil {
		return nil
	}
	return NewServerConn(sconnp)
}

func (s *Server) GetCloseonaccept() int {
	return int(s.p.closeonaccept)
}

func (s *Server) GetLisd() int {
	return int(s.p.lisd)
}

func (s *Server) GetRoot() *ServiceNode {
	return NewServiceNode(s.p.root)
}

func (s *Server) Handler(sconn *ServerConn) {
	C.russ_svr_handler(s.p, sconn.p)
}

func (s *Server) Loop() {
	if int(s.p._type) == RUSS_SVR_TYPE_THREAD {
		s.LoopThread()
	}
	panic("goruss does not support forking server")
}

// threaded server using goroutines
// * sconn is owned
func (s *Server) LoopThread() {
	for s.GetLisd() >= 0 {
		//time.Sleep(1000 * time.Millisecond)
		sconn := s.Accept(ToDeadline(int(s.p.accepttimeout)))
		if s.GetCloseonaccept() == 1 {
			syscall.Close(int(s.GetLisd()))
			s.SetLisd(-1)
		}
		if sconn == nil {
			// error: cannot accept connection
			continue
		}
		go func(s *Server, sconn *ServerConn) {
			s.Handler(sconn)
			sconn.Fatal(RUSS_MSG_NOEXIT, RUSS_EXIT_SYSFAILURE)
			sconn.FreeServerConn()
		}(s, sconn)
	}
}

func (s *Server) SetAccepttimeout(value int) error {
	if int(C.russ_svr_set_accepttimeout(s.p, C.int(value))) < 0 {
		return errors.New("failed to set accepttimeout")
	}
	return nil
}

func (s *Server) SetAutoswitchuser(value int) error {
	if int(C.russ_svr_set_autoswitchuser(s.p, C.int(value))) < 0 {
		return errors.New("failed to set autoswitchuser")
	}
	return nil
}

func (s *Server) SetCloseonaccept(value int) error {
	if int(C.russ_svr_set_closeonaccept(s.p, C.int(value))) < 0 {
		return errors.New("failed to set closeonaccept")
	}
	return nil
}

func (s *Server) SetHelp(help string) error {
	cHelp := C.CString(help)
	defer C.free(unsafe.Pointer(cHelp))

	if int(C.russ_svr_set_help(s.p, cHelp)) < 0 {
		return errors.New("failed to set help")
	}
	return nil
}

func (s *Server) SetLisd(lisd int) error {
	if int(C.russ_svr_set_lisd(s.p, C.int(lisd))) < 0 {
		return errors.New("failed to set lisd")
	}
	return nil
}

func (s *Server) SetRoot(root *ServiceNode) error {
	if int(C.russ_svr_set_root(s.p, root.p)) < 0 {
		return errors.New("failed to set root service node")
	}
	return nil
}

func (s *Server) SetType(typ int) error {
	if int(C.russ_svr_set_type(s.p, C.int(typ))) < 0 {
		return errors.New("failed to set type")
	}
	return nil
}
