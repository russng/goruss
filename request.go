// request.go
//
// license--start
//
// Copyright 2018 John Marshall
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// license--end

package goruss

import (
	"strings"
)

/*
#include <stdlib.h>
#include <russ/priv.h>
*/
import "C"

type Request struct {
	p *C.struct_russ_req
}

func NewRequest(p *C.struct_russ_req) *Request {
	r := &Request{}
	r.p = p
	return r
}

// release underlying object; rarely useful from go
func (r *Request) FreeRequest() {
	C.russ_req_free(r.p)
	r.p = nil
}

func (r *Request) GetArgs() []string {
	args := []string{}

	cArgs := r.p.argv
	for i := 0; ; i++ {
		cValue := getRefCStringArrayValue(cArgs, i)
		args = append(args, C.GoString(cValue))
	}
	return args
}

func (r *Request) GetAttrs() map[string]string {
	attrs := make(map[string]string)

	cAttrs := r.p.attrv
	for i := 0; ; i++ {
		cValue := getRefCStringArrayValue(cAttrs, i)
		attr := C.GoString(cValue)
		t := strings.SplitN(attr, "=", 2)
		attrs[t[0]] = t[1]
	}
	return attrs
}

func (r *Request) GetOp() string {
	return C.GoString(r.p.op)
}

func (r *Request) GetOpnum() int {
	return int(r.p.opnum)
}

func (r *Request) GetProtocolstring() string {
	return C.GoString(r.p.protocolstring)
}

func (r *Request) GetSpath() string {
	return C.GoString(r.p.spath)
}
